<?php

session_start();

function goBack(){
	header("Location: phpChallenge.html");
	exit;
}
	//This isn't neccesary with the form validation on the other side but it is a good precaution now anyway
	if (($_POST['firstName'] == "") || 
	    ($_POST['lastName'] == "") || 
	    ($_POST['dateOfBirth'] == "")||
	    ($_POST['firstCourse'] == "")||
	    ($_POST['secondCourse'] == "")||
	    ($_POST['thirdCourse'] == "")||
	    ($_POST['fourthCourse'] == "")||
	    ($_POST['email'] == "")){
	    goBack();
	}

	if (isset($_FILES['file'])) {
    	$aExtraInfo = getimagesize($_FILES['file']['tmp_name']);
    	$sImage = "data:" . $aExtraInfo["mime"] . ";base64," . base64_encode(file_get_contents($_FILES['file']['tmp_name']));
    }

	$_SESSION['post'] = $_POST;
	$_SESSION['image'] = $_FILES;

	echo
	"<html>
	<head><link rel='stylesheet' type='text/css' href='stylesheet.css'></head>
	<body>
		<p>
			Thanks $_POST[firstName] for enrolling! please check this page over to make sure the information is correct before continuing.
		</p>
		<p>
			Name: $_POST[firstName] $_POST[lastName]<br/>
			Email: $_POST[email]<br/>
			Date of Birth: $_POST[dateOfBirth]<br/>
			Course 1: $_POST[firstCourse]<br/>
			Course 2: $_POST[secondCourse]<br/>
			Course 3: $_POST[thirdCourse]<br/>
			Course 4: $_POST[fourthCourse]<br/>
			Image:<br/>
			".'<img width=\'250\' height=\'200\' src="' . $sImage . '" alt="Your Image" />'."
		</p>
		<form method='post' action='sendForm.php'>
			<input type='submit' name='submit' value='Send Data'>
		</form>
		<button onclick='redirect();'>Go Back</button>
		</body>
		</html>";
	//goBack();
?>

<html>
	<head>
		<title></title>
		<script type="text/javascript">
			function redirect(){
				window.location = 'phpChallenge.html';
			}
		</script>
	</head>
	<body>
	</body>
</html>